#ifndef _DECIDION_TREE_LIB_H_
#define _DECIDION_TREE_LIB_H_
#include<vector>
using namespace std;

/*
* sample item
*/
struct SMPItem{
	SMPItem():_lbl(0){}
	vector<double> _features;
	int _lbl;
	SMPItem(const SMPItem &right){
		this->_features.clear();
		for(vector<double>::const_iterator it=right._features.begin();it!=right._features.end();++it){
			this->_features.push_back(*it);
		}
		this->_lbl=right._lbl;
	
	}
	SMPItem& operator=(const SMPItem &right){
		this->_features.clear();
		for(vector<double>::const_iterator it=right._features.begin();it!=right._features.end();++it){
			this->_features.push_back(*it);
		}
		this->_lbl=right._lbl;
		return *this;
	}
};

#endif//_DECIDION_TREE_LIB_H_
