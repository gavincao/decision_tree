CPP:=g++
CFLAGS:=-c
LDFLAGS:=-g
RM:=rm -rf
PRJNAME:=dt

all:${PRJNAME}

.cpp.o:
	${CPP} ${CFLAGS} $< -g

${PRJNAME}:SMPLoader.o DevideIndicator.o  ID3.o Main.o
	${CPP} ${LDFLAGS} -o $@ $^


.PHONY:clean

clean:
	${RM} *.o
	${RM} ${PRJNAME}
