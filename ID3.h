#ifndef _ID3_H_
#define _ID3_H_
#include<map>
#include<vector>
#include<cmath>
#include<sstream>
#include"DecisionTree.h"
using namespace std;

struct Node{
	Node(){
		_pathVal=0.0;
		_featureIndex=-1;
		_children.clear();
		_remainSmp.clear();
	}
	Node(double pathVal,int featureIndex){
		_featureIndex=featureIndex;
		_pathVal=pathVal;
		_children.clear();
		_remainSmp.clear();
	}
	Node(const Node& right){
		this->_featureIndex=right._featureIndex;
		this->_pathVal=right._pathVal;
		for(vector<Node*>::const_iterator it=right._children.begin();it!=right._children.end();++it){
			this->_children.push_back(*it);
		}
		for(vector<SMPItem>::const_iterator it=right._remainSmp.begin();it!=right._remainSmp.end();++it){
			this->_remainSmp.push_back(*it);
		}
	}
	Node& operator=(const Node& right){
		this->_featureIndex=right._featureIndex;
		this->_pathVal=right._pathVal;
		for(vector<Node*>::const_iterator it=right._children.begin();it!=right._children.end();++it){
			this->_children.push_back(*it);
		}
		for(vector<SMPItem>::const_iterator it=right._remainSmp.begin();it!=right._remainSmp.end();++it){
			this->_remainSmp.push_back(*it);
		}
		return *this;
	} 
	static void serialize(Node *head,stringstream &content);
	vector<SMPItem> _remainSmp;	
	vector<Node*> _children;
	double _pathVal;
	int _featureIndex;
};

class ID3{
public:
	ID3(vector<SMPItem> smps,int minSmp){
		_smps=smps;
		_minSmp=minSmp;
	}
	Node* train(){
		return train(0,_smps);	
	}	
protected:
	Node* train(double parent,vector<SMPItem> &smps);	
	bool canStop(map<double,vector<SMPItem> > &subSets);
	vector<SMPItem> _smps;
	int _minSmp;
};
#endif//_ID3_H_
