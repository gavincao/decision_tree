#include"DevideIndicator.h"
#include<map>
#include<cmath>
#include<iostream>
double DevideIndicator::getEntropy(const vector<SMPItem>& smpSet){
	map< double,int > tmpMap;
	for(int i=0;i<smpSet.size();++i){
		const SMPItem& si=smpSet[i];
		double tmp=(double)si._lbl;
		if(tmpMap.find(tmp)==tmpMap.end()){
			tmpMap[tmp]=1;
		}else{
			tmpMap[tmp]+=1;
		}
	}
	double entropy=0.0;
	for(map<double,int >::iterator it=tmpMap.begin();it!=tmpMap.end();++it){
		double pi=((double)it->second/(double)smpSet.size());
		entropy-=pi*log(pi);
	} 
	return entropy;
}

void DevideIndicator::devideSmpSet(const vector<SMPItem> &smpSet,int featureIndex,map<double,vector<SMPItem> > &subSets){
	map<double,vector<SMPItem> >& tmpMap=subSets;
	for(int i=0;i<smpSet.size();++i){
		const SMPItem& si=smpSet[i];
		double tmp=smpSet[i]._features[featureIndex];
		if(tmpMap.find(tmp)==tmpMap.end()){
			vector<SMPItem> vec;
			vec.push_back(removeFeature(smpSet[i],featureIndex));
			tmpMap[tmp]=vec;
		}else{
			tmpMap[tmp].push_back(removeFeature(smpSet[i],featureIndex));
		}

	}
}

SMPItem DevideIndicator::removeFeature(SMPItem smpItemi,int featureIndex){
	SMPItem smpItem;
	smpItem._lbl=smpItemi._lbl;
	for(int i=0;i<smpItemi._features.size();++i){
		if(i!=featureIndex){
			smpItem._features.push_back(smpItemi._features[i]);
		}
	}
	return smpItem;
}	
