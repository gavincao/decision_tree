#ifndef _DIVIDE_INDICATOR_H_
#define _DIVIDE_INDICATOR_H_
#include"DecisionTree.h"
#include<vector>
#include<map>
using namespace std;

class DevideIndicator{
public:
	/*
	* get entropy of sample set
	*/
	static double getEntropy(const vector<SMPItem>& smpSet);
	/*
	* devide sample set by a given feature
	*/
	static void devideSmpSet(const vector<SMPItem> &smpSet,int featureIndex,map<double,vector<SMPItem> > &subSets);
	/*
	* remove given feature indicator from smpItem
	*/
	static SMPItem removeFeature(SMPItem smpItem,int featureIndex);
};	
#endif//_DIVIDE_INDICATOR_H_
