#include<iostream>
#include<queue>
#include"ID3.h"
#include"DevideIndicator.h"
/*
void Node::serialize(Node* root,stringstream& content){
	content<<"{"<<root->_children.size()<<","<<root->_pathVal<<","<<root->_featureIndex;
	if(root->_remainSmp.size()!=0){
		content<<"(";
		for(vector<SMPItem>::iterator it=root->_remainSmp.begin();
			it!=root->_remainSmp.end();++it)
		{
			content<<it->_lbl<<",";
		}
		content<<")";
	}
	content<<"}";
	for(vector<Node*>::iterator it=root->_children.begin();it!=root->_children.end();++it){
		if(*it!=NULL)
			serialize(*it,content);
	}
}
*/
void Node::serialize(Node* root,stringstream& content){
	if(root==NULL){
		return;
	}
	queue<Node*> q;
	q.push(root);
	while(!q.empty()){
		Node* tmp=q.front();
		q.pop();
		content<<"{"<<tmp->_children.size()<<","<<tmp->_pathVal<<","<<tmp->_featureIndex;
		if(tmp->_remainSmp.size()!=0){
			content<<"(";
			for(vector<SMPItem>::iterator it=tmp->_remainSmp.begin();
				it!=tmp->_remainSmp.end();++it)
			{
				content<<it->_lbl<<",";
			}
			content<<")";
		}
		content<<"}";
	
		for(vector<Node*>::iterator it=tmp->_children.begin();it!=tmp->_children.end();++it){
			q.push(*it);
		}
	}
}
Node* ID3::train(double pathVal,vector<SMPItem> &smps){
	if(smps.size()<=0){
		return NULL;
	}
	if(smps[0]._features.size()==0){
		Node *cur=new Node(pathVal,-1);
		cout<<"no features"<<endl;
		for(vector<SMPItem>::iterator it=smps.begin();it!=smps.end();++it){
			cur->_remainSmp.push_back(*it);
		}
		return cur;
	}
	double baseEntropy=DevideIndicator::getEntropy(smps);
	double maxEntropyGain=0.0;
	int baseNum=smps.size();
	int index=0;
	
	for(int i=0;i<smps[0]._features.size();++i){
		int fID=i;
		double conditionEntropy=0.0;
		map<double,vector<SMPItem> >subSets;
		DevideIndicator::devideSmpSet(smps,fID,subSets);
		for(map<double,vector<SMPItem> >::iterator it=subSets.begin();it!=subSets.end();++it){
			double subEntropy=DevideIndicator::getEntropy(it->second);
			int conditionNum=it->second.size();
			conditionEntropy+=((double)conditionNum/(double)baseNum)*subEntropy;
		}
		if(baseEntropy-conditionEntropy>maxEntropyGain){
			maxEntropyGain=baseEntropy-conditionEntropy;
			index=fID;
		}
	}
	cout<<index<<"\t"<<maxEntropyGain<<endl;		
	map<double,vector<SMPItem> >subSets;
	DevideIndicator::devideSmpSet(smps,index,subSets);
	Node *cur=new Node(pathVal,index);
	if(canStop(subSets)==false){
		for(map<double,vector<SMPItem> >::iterator it=subSets.begin();it!=subSets.end();++it){
			double pV=it->first;
			vector<SMPItem> sS=it->second; 
			cur->_children.push_back(train(pV,sS));
		}
	}else{
		for(vector<SMPItem>::iterator it=smps.begin();it!=smps.end();++it){
			cur->_remainSmp.push_back(*it);
		}
		cur->_featureIndex=-1;
	}
	return cur;
}

bool ID3::canStop(map<double,vector<SMPItem> > &subSets){
	if(subSets.size()==0){
		throw "sub set is in wrong state";
	}

	if(subSets.size()==1){
		return true;
	}else if(subSets.size()==2){
		return false;
		int num=0;
		for(map<double,vector<SMPItem> >::iterator it=subSets.begin();it!=subSets.end();++it){
			num+=(it->second.end()-it->second.begin());	
		}
		if(num<=_minSmp){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

