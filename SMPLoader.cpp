#include<vector>
#include<string>
#include<sstream>
#include<iostream>
#include<fstream>
#include"SMPLoader.h"

using namespace std;

SMPLoader::SMPLoader(const string smpName){
	_smpName=smpName;
}

vector<SMPItem> SMPLoader::load(){
	vector<SMPItem> smpVec;
	ifstream ifs;
	ifs.open(_smpName.c_str(),ios::in);
	char line[256];
	while(!ifs.eof()){
		SMPItem smpItem;
		ifs.getline(line,256);
		string lineTmp(line);
		vector<string> attrs=split(lineTmp,"\t");
		if(attrs.size()<2)continue;
		for(int i=0;i<attrs.size()-1;++i){
			istringstream iss(attrs[i]);
			double tmp;
			iss>>tmp;
			smpItem._features.push_back(tmp);	
		}
		//cout<<endl;
		string lblStr=attrs[attrs.size()-1];
		istringstream iss(lblStr);
		int lbl;
		iss>>lbl;		
		smpItem._lbl=lbl;
		smpVec.push_back(smpItem);
	}
	ifs.close();
	return smpVec;
}

vector<string> SMPLoader::split(const string& str, string sep){
	vector<string> ret_;
	if (str.empty()){
		return ret_;
	}

	string tmp;
	string::size_type pos_begin = str.find_first_not_of(sep);
	string::size_type comma_pos = 0;

	while (pos_begin != string::npos){
		comma_pos = str.find(sep, pos_begin);
		if (comma_pos != string::npos){
			tmp = str.substr(pos_begin, comma_pos - pos_begin);
			pos_begin = comma_pos + sep.length();
		}else{
			tmp = str.substr(pos_begin);
			pos_begin = comma_pos;
		}

		if (!tmp.empty()){
			ret_.push_back(tmp);
			tmp.clear();
		}
	}
	return ret_;
}
